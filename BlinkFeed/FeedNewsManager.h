//
//  FeedNewsManager.h
//  BlinkFeed
//
//  Created by Artur on 15.11.17.
//  Copyright © 2017 Artur. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FeedNews.h"

@interface FeedNewsManager : NSObject

- (FeedNews *)getFeedNewsForPage:(NSInteger)page item:(NSInteger)item;

@end
