//
//  BlinkFeedLayout.m
//  BlinkFeed
//
//  Created by Artur on 14.11.17.
//  Copyright © 2017 Artur. All rights reserved.
//

#import "BlinkFeedLayout.h"

@interface BlinkFeedLayout ()

@property (nonatomic, strong) NSMutableArray *preparedAttributes;

@end

@implementation BlinkFeedLayout

- (instancetype)init {
    self = [super init];
    if (self) {
        self.preparedAttributes = [NSMutableArray array];
    }
    return self;
}

- (CGSize)collectionViewContentSize {
    CGSize result = CGSizeMake([self getCollectionSize].width, [self getCollectionSize].height * [self.collectionView numberOfSections]);
    return result;
}

- (CGSize)getCollectionSize {
    UIEdgeInsets insets = self.collectionView.contentInset;
    CGSize result = CGSizeMake(self.collectionView.bounds.size.width - (insets.left + insets.right), self.collectionView.bounds.size.height - (insets.top + insets.bottom));
    return result;
}

- (CGRect)getItemFrameAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat height = [self getCollectionSize].height;
    CGFloat width  = [self getCollectionSize].width;
    
    CGSize  size   = CGSizeZero;
    CGPoint origin = CGPointZero;

    BOOL isPortrait = height > width;
    if (isPortrait) {
        CGFloat h1 = height / 3;
        CGFloat h3 = height / 3;
        CGFloat h2 = height - h1 - h3;
        
        CGFloat w1 = width / 3;
        CGFloat w2 = width - w1;
        
        if (indexPath.item == 0) {
            size = CGSizeMake(w1, h1);
            origin = CGPointMake(0, 0);
        } else if (indexPath.item == 1) {
            size = CGSizeMake(w2, h1);
            origin = CGPointMake(w1, 0);
        } else if (indexPath.item == 2) {
            size = CGSizeMake(width, h2);
            origin = CGPointMake(0, h1);
        } else if (indexPath.item == 3) {
            size = CGSizeMake(w2, h3);
            origin = CGPointMake(0, h1 + h2);
        } else if (indexPath.item == 4) {
            size = CGSizeMake(w1, h3);
            origin = CGPointMake(w2, h1 + h2);
        } else {
            NSAssert(NO, @"size of the item is undefined, item index is greater then max value");
        }
    } else {
        CGFloat w1 = width / 3;
        CGFloat w3 = width / 3;
        CGFloat w2 = width - w1 - w3;
        
        CGFloat h1 = height / 3;
        CGFloat h2 = height - h1;
        
        if (indexPath.item == 0) {
            size = CGSizeMake(w1, h1);
            origin = CGPointMake(0, 0);
        } else if (indexPath.item == 1) {
            size = CGSizeMake(w1, h2);
            origin = CGPointMake(0, h1);
        } else if (indexPath.item == 2) {
            size = CGSizeMake(w2, height);
            origin = CGPointMake(w1, 0);
        } else if (indexPath.item == 3) {
            size = CGSizeMake(w3, h2);
            origin = CGPointMake(w1 + w2, 0);
        } else if (indexPath.item == 4) {
            size = CGSizeMake(w3, h1);
            origin = CGPointMake(w1 + w2, h2);
        } else {
            NSAssert(NO, @"size of the item is undefined, item index is greater then max value");
        }
    }
    
    CGRect result = CGRectMake(origin.x, origin.y + height * indexPath.section, size.width, size.height);
    return result;
    
}

- (void)prepareLayout {
    
    if (self.preparedAttributes.count == 0) {
        
        NSInteger sectionsCount = [self.collectionView numberOfSections];
        
        for (NSInteger section = 0; section < sectionsCount; section++) {
            
            NSInteger itemsCount = [self.collectionView numberOfItemsInSection:section];
            
            NSMutableArray *itemsInSection = [NSMutableArray array];
            
            for (NSInteger item = 0; item < itemsCount; item++) {
                
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:item inSection:section];
                CGRect itemFrame = [self getItemFrameAtIndexPath:indexPath];
                
                UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
                attributes.frame = itemFrame;
                [itemsInSection addObject:attributes];
                
            }
            
            [self.preparedAttributes addObject:itemsInSection];
        }
        
    }
    
}

- (NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect {
    
    NSMutableArray<UICollectionViewLayoutAttributes *> *result = [NSMutableArray array];
    
    CGRect visibleRect;
    visibleRect.origin = self.collectionView.contentOffset;
    visibleRect.size   = [self getCollectionSize];

    CGFloat sectionHeight = [self getCollectionSize].height;

    for (NSInteger section = 0; section < self.preparedAttributes.count; section++) {
        NSArray *itemsInSection = [self.preparedAttributes objectAtIndex:section];
        for (NSInteger item = 0; item < itemsInSection.count; item++) {
            UICollectionViewLayoutAttributes *attributes = [itemsInSection objectAtIndex:item];
            if (CGRectIntersectsRect(attributes.frame, rect)) {
                
                CGFloat yShiftInSection = CGRectGetMinY(visibleRect) - sectionHeight * floorf(CGRectGetMinY(visibleRect) / sectionHeight);
                CGFloat rotationAngle = 0.26 * M_PI * sqrtf(1 - ABS(1 - 2 * yShiftInSection / sectionHeight));
                
                CGPoint itemCenter    = CGPointMake(CGRectGetMidX(attributes.frame), CGRectGetMidY(attributes.frame));
                CGPoint rotationPoint = CGPointMake(CGRectGetMidX(visibleRect), CGRectGetMaxY(attributes.frame));

                CATransform3D transform = CATransform3DIdentity;
                CGFloat tx = itemCenter.x - rotationPoint.x;
                CGFloat ty = itemCenter.y - rotationPoint.y;
                transform = CATransform3DTranslate(transform, tx, ty, 0.0);
                transform.m34 = -1.0/500.0;
                transform = CATransform3DRotate(transform, rotationAngle, 1.0, 0.0, 0.0);

                CGAffineTransform m = CGAffineTransformIdentity;
                m = CGAffineTransformTranslate(m, -tx, -ty);
                CATransform3D m3d = CATransform3DMakeAffineTransform(m);
                transform = CATransform3DConcat(transform, m3d);
                attributes.transform3D = transform;
                
                [result addObject:attributes];

            }
        }
    }
    
    
    return result;

}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *itemsInSection = [self.preparedAttributes objectAtIndex:indexPath.section];
    return [itemsInSection objectAtIndex:indexPath.item];
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return YES;
}

# pragma mark - Utility

- (void)resetPreparedAttributes {
    [self.preparedAttributes removeAllObjects];
}

@end
