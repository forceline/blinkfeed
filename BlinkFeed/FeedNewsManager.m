//
//  FeedNewsManager.m
//  BlinkFeed
//
//  Created by Artur on 15.11.17.
//  Copyright © 2017 Artur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeedNewsManager.h"

@interface FeedNewsManager ()

@property (nonatomic, strong) NSMutableArray *itemIDsInSections;

@end

@implementation FeedNewsManager

- (instancetype)init {
    self = [super init];
    if (self) {
        self.itemIDsInSections = [NSMutableArray array];
    }
    return self;
}

- (FeedNews *)getFeedNewsForPage:(NSInteger)page item:(NSInteger)item {
    
    NSInteger itemID = [self getRandomItemIDForPage:page item:item];
    
    FeedNews *news = [self getNewsWithID:itemID];
    
    return news;
    
}

- (FeedNews *)getNewsWithID:(NSInteger)newsID {
    
    FeedNews *result = nil;
    
    NSString *title = nil;
    UIImage *image = nil;
    NSString *url = nil;
    
    if (newsID == 0) {
        title = @"Риккардо: Я мечтаю стать чемпионом мира";
        image  = [UIImage imageNamed:@"image00"];
        url = @"https://www.f1news.ru/news/f1-125451.html";
    } else if (newsID == 1) {
        title = @"Мартин Брандл об итогах Гран При Бразилии";
        image  = [UIImage imageNamed:@"image01"];
        url = @"https://www.f1news.ru/news/f1-125443.html";
    } else if (newsID == 2) {
        title = @"Вандорн: Red Bull будет для нас ориентиром в 2018-м";
        image  = [UIImage imageNamed:@"image02"];
        url = @"https://www.f1news.ru/interview/vandoorne/125448.shtml";
    } else if (newsID == 3) {
        title = @"В Pirelli подтвердили выбор шин для Гран При Абу-Даби";
        image  = [UIImage imageNamed:@"image03"];
        url = @"https://www.f1news.ru/news/f1-125438.html";
    } else {
        title = @"Хэмилтон: Я ещё не верю, что выиграл титул";
        image  = [UIImage imageNamed:@"image04"];
        url = @"https://www.f1news.ru/news/f1-124765.html";
    }
    
    result = [[FeedNews alloc] initWithData:[NSDictionary dictionaryWithObjectsAndKeys:
                                             title, @"title",
                                             image, @"image",
                                             url,@"url",
                                             nil]];
    
    return result;

}

- (NSInteger)getRandomItemIDForPage:(NSInteger)page item:(NSInteger)item {
    
    NSInteger result = 0;
    
    if (page < self.itemIDsInSections.count) {
        NSArray * itemIDs = [self.itemIDsInSections objectAtIndex:page];
        result = [[itemIDs objectAtIndex:item] integerValue];
    } else {
        NSMutableArray *itemIDs = [NSMutableArray array];
        while (itemIDs.count < 5) {
            NSNumber *rand = [NSNumber numberWithInteger:[self randomValueBetween:0 and:4]];
            if ([itemIDs containsObject:rand] == NO) {
                [itemIDs addObject:rand];
            }
            
        }
        [self.itemIDsInSections addObject:itemIDs];
        result = [[itemIDs objectAtIndex:item] integerValue];
    }
    
    return result;
    
}

- (NSInteger)randomValueBetween:(NSInteger)min and:(NSInteger)max {
    return (NSInteger)(min + arc4random_uniform((uint32_t)(max - min + 1)));
}

@end
