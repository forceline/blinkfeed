//
//  FeedNews.h
//  BlinkFeed
//
//  Created by Artur on 14.11.17.
//  Copyright © 2017 Artur. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface FeedNews : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSString *url;

- (instancetype)initWithData:(NSDictionary *)data;

@end
