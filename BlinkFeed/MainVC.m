//
//  MainVC.m
//  BlinkFeed
//
//  Created by Artur on 14.11.17.
//  Copyright © 2017 Artur. All rights reserved.
//

#import "MainVC.h"
#import "FeedCell.h"
#import "FeedNews.h"
#import "FeedNewsManager.h"

#import "BlinkFeedLayout.h"

@interface MainVC () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *feedView;
@property (strong, nonatomic) FeedNewsManager *feedNewsManager;

@end

@implementation MainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupModel];
    [self setupAppearence];
    [self setupCollectionView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    
    NSInteger section = 0;
    
    NSArray *indexPaths = [self.feedView indexPathsForVisibleItems];
    if (indexPaths) {
        NSIndexPath *indexPath = [indexPaths firstObject];
        section = indexPath.section;
    }
    
    __weak typeof(self) weakSelf = self;
    
    BlinkFeedLayout *layout = (BlinkFeedLayout *)self.feedView.collectionViewLayout;
    [layout resetPreparedAttributes];

    [coordinator animateAlongsideTransition:nil completion:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:section];
        [weakSelf.feedView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
        
    }];
    
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
}

#pragma mark - Utility

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)setupAppearence {
    self.view.backgroundColor = [UIColor blackColor];
    self.feedView.backgroundColor = [UIColor blackColor];
}

- (void)setupCollectionView {
    self.feedView.delegate = self;
    self.feedView.dataSource = self;
    
    [self.feedView registerNib:[UINib nibWithNibName:NSStringFromClass([FeedCell class]) bundle:nil] forCellWithReuseIdentifier:[FeedCell reuseID]];
    
    self.feedView.collectionViewLayout = [[BlinkFeedLayout alloc] init];
}

- (void)showMessageWithTitle:(NSString *)title text:(NSString *)text {
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:text
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* button = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    
                                }];
    
    [alert addAction:button];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - UICollectionView delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 10;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 5;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    FeedNews *news = [self.feedNewsManager getFeedNewsForPage:indexPath.section item:indexPath.item];
    
    FeedCell *result = [self.feedView dequeueReusableCellWithReuseIdentifier:[FeedCell reuseID] forIndexPath:indexPath];
    [result setupWithNews:news];
    return result;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *title   = @"Position of the item";
    NSString *text = [NSString stringWithFormat:@"Page - %ld, item - %ld", (long)indexPath.section, (long)indexPath.item];
    [self showMessageWithTitle:title text:text];
}

#pragma mark - UIScrollView delegate

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    
}

#pragma mark - Data

- (void)setupModel {
    self.feedNewsManager = [[FeedNewsManager alloc] init];
}

@end
