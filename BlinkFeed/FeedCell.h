//
//  FeedCell.h
//  BlinkFeed
//
//  Created by Artur on 14.11.17.
//  Copyright © 2017 Artur. All rights reserved.
//

#import <UIKit/UIKit.h>
@class FeedNews;

@interface FeedCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (strong, nonatomic) FeedNews *news;

+ (NSString *)reuseID;

- (void)setupWithNews:(FeedNews *)news;

@end
