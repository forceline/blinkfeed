//
//  FeedNews.m
//  BlinkFeed
//
//  Created by Artur on 14.11.17.
//  Copyright © 2017 Artur. All rights reserved.
//

#import "FeedNews.h"

@implementation FeedNews

- (instancetype)initWithData:(NSDictionary *)data {
    
    self = [super init];
    if (self) {
        self.title = [data objectForKey:@"title"];
        self.image = [data objectForKey:@"image"];
        self.url   = [data objectForKey:@"url"];
    }
    return self;
    
}

@end
